package application;

import static application.Subjects.BD;
import static application.Subjects.BSiUI;
import static application.Subjects.JAVA;
import static application.Subjects.PEA;
import static application.Subjects.SDiZO;

import java.util.HashMap;
import java.util.List;

public class SummingRow extends Student{

	public SummingRow(){
		super("Average");
	}
	
	public SummingRow(List<Student> students){
		super("Average");
		
		this.setGrades(new HashMap<String, Float>());
		
		this.getGrades().put(JAVA.toString(), countAverageForSubject(JAVA, students));
		this.getGrades().put(BD.toString(), countAverageForSubject(BD, students));
		this.getGrades().put(BSiUI.toString(), countAverageForSubject(BSiUI, students));
		this.getGrades().put(PEA.toString(), countAverageForSubject(PEA, students));
		this.getGrades().put(SDiZO.toString(), countAverageForSubject(SDiZO, students));
	}
	
	private float countAverageForSubject(Subjects subject, List<Student> students){
		
		return (float) students.stream()
								.mapToDouble(student -> student.getGrade(subject.toString()))
								.average()
								.getAsDouble();
	}
}
