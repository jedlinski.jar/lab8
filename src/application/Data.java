package application;

import static java.util.Arrays.asList;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
@XmlAccessorType(FIELD)
public class Data {

	private List<Student> students = new ArrayList<>(asList(new Student("209940"), 
															new Student("209666"),
															new Student("123456")));

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
}
