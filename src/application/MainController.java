package application;

import static application.Subjects.BD;
import static application.Subjects.BSiUI;
import static application.Subjects.JAVA;
import static application.Subjects.PEA;
import static application.Subjects.SDiZO;
import static java.lang.Float.parseFloat;
import static java.util.stream.Collectors.toList;
import static javafx.collections.FXCollections.observableArrayList;
import static javafx.scene.control.cell.TextFieldTableCell.forTableColumn;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class MainController {

	private Data data = new Data();
	private SummingRow sumRow = new SummingRow();

	@FXML
	private TableView<Student> studentsTable;

	@FXML
	private Button button, newStudentButton;

	@FXML
	private TextField newStudentIndex;

	@FXML
	private TableColumn<Student, String> nameColumn, javaColumn, javaPercent, bsiuiColumn, bsiuiPercent, bdColumn,
			bdPercent, peaColumn, peaPercent, sdizoColumn, sdizoPercent;

	private ObservableList<Student> students = observableArrayList(data.getStudents());

	@FXML
	protected void initialize() {

		students.add(sumRow);
		studentsTable.setItems(students);
		studentsTable.setEditable(true);

		studentsTable.setRowFactory(tv -> {
			PseudoClass lastLinePC = PseudoClass.getPseudoClass("last-line");
			TableRow<Student> row = new TableRow<>();
			row.indexProperty().addListener((obs, oldIndex, newIndex) -> {
				row.pseudoClassStateChanged(lastLinePC, newIndex.intValue() == students.size() - 1);
			});
			
			return row;
		});

		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

		configureColumn(javaColumn, JAVA);
		configurePercentColumn(javaPercent, JAVA);

		configureColumn(bsiuiColumn, BSiUI);
		configurePercentColumn(bsiuiPercent, BSiUI);

		configureColumn(bdColumn, BD);
		configurePercentColumn(bdPercent, BD);

		configureColumn(peaColumn, PEA);
		configurePercentColumn(peaPercent, PEA);

		configureColumn(sdizoColumn, SDiZO);
		configurePercentColumn(sdizoPercent, SDiZO);

		studentsTable.refresh();
	}

	private void configurePercentColumn(TableColumn<Student, String> column, Subjects subject) {

		column.setCellFactory(forTableColumn());
		column.setCellValueFactory(percentColumnValueFactory(subject));
	}

	private Callback<CellDataFeatures<Student, String>, ObservableValue<String>> percentColumnValueFactory(
			Subjects subject) {

		return (CellDataFeatures<Student, String> param) -> countStudentPercentage(subject, param);
	}

	private ObservableValue<String> countStudentPercentage(Subjects subject, CellDataFeatures<Student, String> param) {

		float thisGrade = param.getValue().getGrade(subject.toString());
		int theSameOrLess = countStudentsWithLessOrTheSameGrade(subject, thisGrade);

		return new ReadOnlyStringWrapper(String.format("%.2f %%", 100.0 * theSameOrLess / data.getStudents().size()));
	}

	private int countStudentsWithLessOrTheSameGrade(Subjects subject, float thisStudentGrade) {

		return data.getStudents().stream().filter(student -> student.getGrade(subject.toString()) <= thisStudentGrade)
				.collect(toList()).size();
	}

	private void configureColumn(TableColumn<Student, String> column, Subjects subject) {

		column.setCellValueFactory(
				cellData -> new ReadOnlyStringWrapper(cellData.getValue().getGradeAsString(subject.toString())));
		column.setCellFactory(forTableColumn());
	}

	@FXML
	protected void editGrade(CellEditEvent<Student, String> event) {

		int index = event.getTablePosition().getRow();
		String column = ((TableColumn) event.getSource()).getText();

		data.getStudents().get(index).getGrades().put(column, parseFloat(event.getNewValue()));

		students.remove(sumRow);
		sumRow = new SummingRow(data.getStudents());
		students.add(sumRow);

		studentsTable.refresh();
	}

	@FXML
	protected void addNewStudent(ActionEvent event) {

		if ("".equals(newStudentIndex.getText())) {
			return;
		}

		Student newStudent = new Student(newStudentIndex.getText());

		data.getStudents().add(newStudent);

		students.remove(sumRow);
		students.add(newStudent);
		sumRow = new SummingRow(data.getStudents());
		students.add(sumRow);
	}

	@FXML
	protected void serializeData(ActionEvent event) throws JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(data, new File("data.xml"));
	}

	@FXML
	protected void deserializeData(ActionEvent event) throws JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		data = (Data) jaxbUnmarshaller.unmarshal(new File("data.xml"));

		students = observableArrayList(data.getStudents());

		studentsTable.setItems(students);

		studentsTable.refresh();
	}
}
